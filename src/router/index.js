import { createRouter, createWebHistory } from 'vue-router';
import ListTasks from '../views/ListTasks.vue';
import CreateTask from '../views/CreateTask.vue';
import EditTask from '../views/EditTask.vue';
import ShowTask from '../views/ShowTask.vue';

const routes = [
  { path: '/', component: ListTasks },
  { path: '/create', component: CreateTask },
  { path: '/edit/:id', component: EditTask,  },
  { path: '/show/:id', component: ShowTask },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
